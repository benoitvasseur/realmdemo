//
//  AppDelegate.h
//  RealmDemo
//
//  Created by Benoit Vasseur on 18/04/16.
//  Copyright © 2016 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

