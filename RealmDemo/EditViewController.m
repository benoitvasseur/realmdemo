//
//  EditViewController.m
//  RealmDemo
//
//  Created by Benoit Vasseur on 18/04/16.
//  Copyright © 2016 Cardiweb. All rights reserved.
//

#import "EditViewController.h"
#import "Person.h"
#import "Familly.h"

@interface EditViewController () <UITableViewDataSource, UITabBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *famillyField;

@property (nonatomic, strong) RLMResults<Familly *> *famillies;

@end

@implementation EditViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    _famillyField.text = _familly.name;
    _nameField.text = _person.name;
    
    self.famillies = [Familly allObjects];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)save:(id)sender {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    Person *person = _person ? _person : [[Person alloc] init];
    Familly *familly = nil;
    if (!_familly || ![_familly.name isEqualToString:_famillyField.text]) {
        familly = [[Familly alloc] init];
        familly.name = _famillyField.text;
    } else {
        familly = _familly;
    }
    if (!_person) {
        person.idPerson = @([Person allObjects].count);
    }
    
    person.familly = familly;
    person.name = _nameField.text;
    
    [realm addOrUpdateObject:familly];
    [realm addOrUpdateObject:person];
    
    [realm commitWriteTransaction];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _famillies.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.familly = _famillies[indexPath.row];
    _famillyField.text = _familly.name;
    [_tableview reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"CellId";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    Familly *familly = _famillies[indexPath.row];
    cell.textLabel.text = familly.name;
    
    if ([familly.name isEqualToString:_familly.name]) {
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    } else {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    return cell;
}

@end
