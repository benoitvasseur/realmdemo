//
//  Familly.m
//  RealmDemo
//
//  Created by Benoit Vasseur on 18/04/16.
//  Copyright © 2016 Cardiweb. All rights reserved.
//

#import "Familly.h"

@implementation Familly
- (NSArray<Person *> *)persons {
    return [self linkingObjectsOfClass:@"Persons" forProperty:@"familly"];
}

@end
