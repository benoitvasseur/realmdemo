//
//  EditViewController.h
//  RealmDemo
//
//  Created by Benoit Vasseur on 18/04/16.
//  Copyright © 2016 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Person;
@class Familly;
@interface EditViewController : UIViewController
@property (nonatomic, strong) Person *person;
@property (nonatomic, strong) Familly *familly;
@end
