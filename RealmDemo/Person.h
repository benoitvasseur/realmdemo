//
//  Person.h
//  RealmDemo
//
//  Created by Benoit Vasseur on 18/04/16.
//  Copyright © 2016 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>


RLM_ARRAY_TYPE(Person);

@class Familly;
@interface Person : RLMObject
@property NSString *name;
@property NSNumber<RLMInt> *idPerson;
@property Familly *familly;
@end
