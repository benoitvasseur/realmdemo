//
//  Familly.h
//  RealmDemo
//
//  Created by Benoit Vasseur on 18/04/16.
//  Copyright © 2016 Cardiweb. All rights reserved.
//

#import <Realm/Realm.h>
#import "Person.h"

@interface Familly : RLMObject
@property NSString *name;
@property (readonly) NSArray<Person *> *persons;
@end
